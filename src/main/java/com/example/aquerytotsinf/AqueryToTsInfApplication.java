package com.example.aquerytotsinf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AqueryToTsInfApplication {

    public static void main(String[] args) {
        SpringApplication.run(AqueryToTsInfApplication.class, args);
    }

}
