package com.example.aquerytotsinf.adapter;


import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AqueryController {

    private String anoStart = "\t/**";
    private String anoStar = "\t* ";
    private String anoEnd = "\t*/";

    @PostMapping(value = "/aquery/ts/inf", consumes = MediaType.ALL_VALUE)
    @ResponseBody
    public String aqueryGenerate(@RequestParam ("file") MultipartFile file) throws IOException {

        byte[] fileBytes =  file.getBytes();
        String coverted = new String(fileBytes);
        String jsonRaw = coverted; //txt 파일 추출

        //스네이크 케이스 인터페이스 명 추출
        String[] forSnakeArr =  jsonRaw.split(" = ");
        String infSnakeNm = snakeToCamel(forSnakeArr[0].split(" ")[1]);

        //인터페이스 명은 파스칼 케이스
        infSnakeNm = infSnakeNm.substring(0,1).toUpperCase() + infSnakeNm.substring(1);

        //중괄호 및 좌우 공백 제거
        String realContents = forSnakeArr[1].substring(0, forSnakeArr[1].length()).replace("{","").replace("}","").trim();

        //논리명 물리명 분리
        String[] splitEngKor = realContents.split(",");

        List<List<String>> erdInfo = genFieldName(splitEngKor);
        String rtnVal = tfToTsInf(erdInfo,infSnakeNm);
        System.out.println(rtnVal);

        HashMap<String, String> rtnMap = new HashMap<>();
        rtnMap.put("rtnVal","");

        return rtnVal;

    }

    public String snakeToCamel(String str) {
        str = str.indexOf("_") != -1
                ? str.substring(0, str.indexOf("_")) +
                Arrays.stream(str.substring(str.indexOf("_") + 1).split("_"))
                        .map(s -> Character.toUpperCase(s.charAt(0)) + s.substring(1)).collect(Collectors.joining())
                : str;

        return str;
    }

    //텍스트 가공해서 물리, 논리, 타입 리스트 분리함
    public List<List<String>> genFieldName(String[] splitEngKor) {

        List<List<String>> rtnVal = new ArrayList<>();

        List<String> pscList = new ArrayList<>();
        List<String> typeList = new ArrayList<>();
        List<String> lgcList = new ArrayList<>();

        for (int i=0; i < splitEngKor.length; i++) {
            String temp = splitEngKor[i];
            if (i == 0) {
                List<String> pscTempInfo = genPscType(temp.trim());
                pscList.add(pscTempInfo.get(0)+"?");
                typeList.add(pscTempInfo.get(1));
            }

            if(i > 0 && i < splitEngKor.length - 1) {
                String lgcTempNm = temp.trim().split("     ")[0];
                String phyTempNm = temp.trim().split("     ")[1];
                List<String> pscTempInfo = genPscType(phyTempNm);
                pscList.add(pscTempInfo.get(0)+"?");
                typeList.add(pscTempInfo.get(1));

                String lgcNm = lgcTempNm.substring(3,lgcTempNm.length()).replace(" ","_");
                lgcList.add(lgcNm);
            }

            if(i == splitEngKor.length - 1) {
                String lgcNm = temp.trim().substring(3,temp.trim().length()).replace(" ","_");
                lgcList.add(lgcNm);

            }

        }

        rtnVal.add(pscList);
        rtnVal.add(typeList);
        rtnVal.add(lgcList);

        return rtnVal;
    }

    // 첫 번째는 필드명, 두 번째는 type명
    public List<String> genPscType(String str) {
        String psc = str.split(":")[0];
        String type = (str.contains(": \'")) ? "string" : "number";

        List<String> rtnVal = new ArrayList<>();
        rtnVal.add(snakeToCamel(psc));
        rtnVal.add(type);

        return rtnVal;
    }

    public String tfToTsInf(List<List<String>> info, String InfNm) {
        StringBuilder rtnVal = new StringBuilder();

        List<String> pscList = info.get(0);
        List<String> typeList = info.get(1);
        List<String> lgcList = info.get(2);

        rtnVal.append("export interface " + InfNm + " {" + "\n");

        for (int i=0; i < pscList.size(); i++) {

            rtnVal.append("\n");
            rtnVal.append(anoStart + "\n");
            rtnVal.append(anoStar + "@type {" + typeList.get(i).trim() + "}" + "\n");
            rtnVal.append(anoStar + "@memberof " + InfNm + "\n");
            rtnVal.append(anoStar + "@name " + lgcList.get(i).trim() + "\n");
            rtnVal.append(anoEnd + "\n" + "\t" + pscList.get(i).trim() + " : " + typeList.get(i).trim()+";" + "\n");
        }

        rtnVal.append("\n" + "};");

        return rtnVal.toString();
    }
}
